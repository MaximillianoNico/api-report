const chai = require('chai');
const http = require('chai-http');
const server = require('../server');

const expect = chai.expect;

chai.use(http);
const Controller = require('../src/article/v1/controller')

describe("test component Articles", () =>{
    before(()=>{
        // require('../server')
    })
    describe("Controller", () => {
        it("#Get articles", async ()=>{
            const res = await chai.request('http://localhost:8080/api/v1')
                .get('/article')
                .then((res) => res.body)
        })

        it("#Get article by id", async ()=>{
            const id ='5be307c505f6812ccc08f286';
            const res = await chai.request('http://localhost:8080/api/v1')
                .get(`/article/${id}`)
                .then((res) => res.body)
        })
    })
})