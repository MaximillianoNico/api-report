FROM node:9

WORKDIR /src/app

COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

EXPOSE 8080

CMD [ "npm run", "pm2:start" ]

