var express = require('express');
var app = express();
const server = require('api-core');
const autoloader = require('./config/autoloader');

server.start(app,autoloader,process.env.LOCAL_PORT)
    .then((res)=>{
        console.log(res);
    })
    .catch((err)=>{
        // console.log(err);
    });


module.exports = app;