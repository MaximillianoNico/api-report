module.exports = {
    // directory api
    dir: 'src',
    // add api routers
    modules: ['report', 'fragmentation'],
    // use current version api
    version: 'v1',
  };
  