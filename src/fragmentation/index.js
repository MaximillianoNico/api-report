var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

const Albums = require('./v1/model');
const Controller = require('./v1/controller');

router.use( bodyParser.json() );     
router.use(bodyParser.urlencoded({
  extended: true
}));

router.get('/', Controller.getAssets);


module.exports = router;