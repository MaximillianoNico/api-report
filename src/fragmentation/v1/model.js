const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moduleName = "assets";

const Assets = new Schema({
    id_user:{
        type:String,
        required:true
    },
    metadata:{
        typeData:{
            type:String,
            required:true
        },
        section:{
            type:String,
            required:true
        },
        slug:{
            type:String,
            required:true
        },
        title:{
            type:String,
            required:true
        },
        tags:[String]
    },
    detail:{
        type:String,
        required:true
    }
},{
    collection:moduleName
})

Assets.statics ={
    insertData: function (data){
        return this.insertMany(data, (err, res) => {
            if(!res) return "Can't Insert to MONGO";
            if(err) return new Error(err)
            return res;
        })
    },
    removeData: function(id){
        return this.remove({ _id : id }, (err) => {
            if(!err) return "success remove data";
            return new Error(err);
        })
    }
}

module.exports = mongoose.model('assets',Assets);