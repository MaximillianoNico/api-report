var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

const Controller = require('./v1/controller');

router.use( bodyParser.json() );     
router.use(bodyParser.urlencoded({
  extended: true
}));

router.get('/create', Controller.createMasterData);
router.get('/create/report', Controller.createMasterData);

module.exports = router;