const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moduleName = "PIC";

const PIC = new Schema({
    divisi:{
        type:String,
        required:true
    },
    PIC:{
        type:String,
        required:true
    }
},{
    collection:moduleName
})

PIC.statics ={
    create: function (data){
        return this.insertMany(data, (err, res) => {
            if(!res) return "Can't Insert to MONGO";
            if(err) return new Error(err)
            return res;
        })
    }
}

module.exports = mongoose.model('PIC',PIC);