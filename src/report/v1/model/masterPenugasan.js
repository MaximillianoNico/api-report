const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moduleName = "report-assign";

const ReportAssign = new Schema({
    cabang:{
        type:String,
        required:true
    },
    manager:{
        type:String,
        required:true
    },
    supervisor:{
        type:String,
        required:true
    },
    staff:{
        type:String,
        required:true
    }
},{
    collection:moduleName
})

ReportAssign.statics ={
    create: function (data){
        return this.insertMany(data, (err, res) => {
            if(!res) return "Can't Insert to MONGO";
            if(err) return new Error(err)
            return res;
        })
    }
}

module.exports = mongoose.model('ReportAssign',ReportAssign);