const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moduleName = "Pegawai";

const Pegawai = new Schema({
    nip:{
        type:String,
        required:true
    },
    nama_pegawai:{
        type:Date,
        required:true
    },
    jabatan:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    phone:{
        type:String,
        required:true
    },
    cabang:{
        type:String,
        required:true
    }
},{
    collection:moduleName
})

Pegawai.statics ={
    create: function (data){
        return this.insertMany(data, (err, res) => {
            if(!res) return "Can't Insert to MONGO";
            if(err) return new Error(err)
            return res;
        })
    }
}

module.exports = mongoose.model('Pegawai',Pegawai);