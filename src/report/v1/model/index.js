const Pegawai = require('./masterPegawai')
const Assign = require('./masterPenugasan')
const PIC = require('./PIC');
const Report = require('./Report')

module.exports = {
    MasterPegawai,
    Assign,
    PIC,
    Report
}