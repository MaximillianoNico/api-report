const { MasterPegawai, Assign, PIC, Report } = require('./model');

const controller ={ 
    
  createMasterData : function (request, response){
    try{
      const { nip, nama_pegawai, jabatan, email, phone, cabang } = request.body.employee;
      const { cabang_assign, manager, supervisor, staff } = request.body.assign;
      const { divisi, PIC_id } = request.body.assign;
      
      const payload ={
        pegawai:{
          nip: nip,
          nama_pegawai: nama_pegawai,
          jabatan: jabatan,
          email: email,
          phone: phone,
          cabang: cabang,
        },
        assign:{
          cabang: cabang_assign,
          manager: manager,
          staff: staff,
          supervisor: supervisor
        },
        PIC_report:{
          divisi: divisi,
          PIC: PIC
        }
      }

      MasterPegawai.insertMany(payload.pegawai)
        .then((res) => {console.log(res)})
        .catch((err) => {
          throw err
        })

      Assign.insertMany(payload.assign)
        .then((res) => {console.log(res)})
        .catch((err) => {
          throw err
        })

      PIC.insertMany(payload.PIC_report)
        .then((res) => { console.log(res)})
        .catch((err) => {
          throw err
        })

      return response.status(200).send({res,method:"POST"})
    }catch(err){
      return response.status(400).send({err,method:"POST"})
    }
  },
  createReport : function (request, response){
    const { idAssign, PIC, Observation, planFeature }
    const payload = {
      idAssign: idAssign,
      PIC: PIC,
      Observation: Observation,
      planFeature: planFeature
    }
    Report.insertMany(payload)
      .then((res) => {
        return response.status(200).send({res,method:'POST'})
      })
      .catch(err => {
        return response.status(400).send({err,method:'POST'})
      })
  }
}
module.exports = controller;